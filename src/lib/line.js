import Chart from './chart';

export default class Line extends Chart {
    constructor(element, options, dataset) {
        super(element, options, dataset);
    }

    getLineGradient(opacity = 1) {
        let gradient = this.ctx.createLinearGradient(0, 0, this.width, 0);

        this.options.lineGradientPoints.forEach((point) => {
            gradient.addColorStop(point[0], Chart.hexRgb(point[1], opacity));
        });

        return gradient;
    }

    getActiveLabelGradient(y, height) {
        let gradient = this.ctx.createLinearGradient(0, y, 0, height);

        this.options.activeLabelGradient.forEach((point) => {
            gradient.addColorStop(point[0], Chart.hexRgb(point[1], point[2]));
        });

        return gradient;
    }

    draw(showLabels = false) {
        this.ctx.clearRect(0, 0, this.width, this.height);
        this.drawDots();
        this.startUnderLineClip();
        this.drawActiveLabel();
        this.endUnderLineClip();
        this.drawLine();
        this.drawPoints();
        this.drawLabels();

        if(showLabels) this.drawValues();
    }

    clickHandler(e) {
        let range = this.columnRanges.find((range) => {
            return range[0] <= e.offsetX && range[1] > e.offsetX;
        });

        this.activeValue = this.columnRanges.indexOf(range);
        this.draw(true);
    }

    drawPoints() {
        let points = this.getLinePoints();

        points.forEach((point, index) => {
            this.ctx.beginPath();
            this.ctx.lineWidth = 3;
            this.ctx.arc(point[0], point[1], 4, 0, 2 * Math.PI, true);
            this.ctx.strokeStyle = this.getLineGradient();
            this.ctx.stroke();
            this.ctx.fillStyle = (index - 1 == this.activeValue) ? this.getLineGradient() : '#0f2a38';
            this.ctx.fill();
            this.ctx.closePath();
        });
    }

    drawLine() {
        let points = Chart.joinPoints(this.getLinePoints());

        this.ctx.beginPath();
        this.ctx.strokeStyle = this.getLineGradient();
        this.ctx.moveTo(points[0], points[1]);
        this.ctx.curve(points);
        this.ctx.lineWidth = 2;
        this.ctx.stroke();
        this.ctx.closePath();
    }

    startUnderLineClip() {
        this.ctx.save();

        let points = this.getLinePoints();
        this.ctx.beginPath();
        this.ctx.curve(Chart.joinPoints(points));
        this.ctx.lineTo(points.slice(-1)[0][0], this.height + 10);
        this.ctx.lineTo(-10, this.height + 10);
        this.ctx.lineTo(points.slice(0, 1)[0][0], points.slice(0, 1)[0][1]);
        this.ctx.strokeStyle = this.getLineGradient(0.5);
        this.ctx.lineWidth = 4;
        this.ctx.stroke();
        this.ctx.clip();
    }

    endUnderLineClip() {
        this.ctx.restore();
    }

    drawActiveLabel() {
        let points = this.getLinePoints(),
            columnWidth = this.columnWidth,
            x1 = this.activeValue * columnWidth;

        this.ctx.fillStyle = this.getActiveLabelGradient(points[this.activeValue][1], this.height);
        this.ctx.fillRect(x1, 0, columnWidth, this.height);
    }

    drawValues() {
        let points = this.getLinePoints(true),
            point = points[this.activeValue],
            pointPrev = points[this.activeValue - 1],
            pointNext = points[this.activeValue + 1],
            datasetValues = this.dataset.values,
            datasetValue = datasetValues[this.activeValue + 1],
            datasetValuePrev = datasetValues[this.activeValue],
            datasetValueNext = datasetValues[this.activeValue + 2],
            diff = datasetValue - datasetValues[this.activeValue],
            yOffset = 20,
            xMargin = 20,
            textSeparatorWidth = 5,
            textHeight = 5,
            x = point[0],
            y = point[1] + textHeight,
            position = 'top',
            threshold = 20;


        if(pointNext && pointPrev) {
            // Если предыдущая точка и следующая выше
            if(datasetValuePrev < datasetValue && datasetValueNext < datasetValue) {
                position = 'bottom';
            } else if(datasetValuePrev > datasetValue && datasetValueNext > datasetValue) {
                position = 'top';
            } else {
                if(datasetValuePrev <= datasetValue && datasetValueNext >= datasetValue) {
                    let diffY = Math.abs(100 / ((this.height - this.heightMargins) / (point[1] - pointPrev[1])));

                    if(diffY >= threshold || datasetValueNext == datasetValue) {
                        position = 'bottom';
                        if (Math.abs(point[1] - pointNext[1]) > yOffset + textHeight) {
                            y = pointNext[1];
                        }
                    } else {
                        position = 'top';
                        if(Math.abs(point[1] - pointPrev[1]) > yOffset + textHeight) {
                            y = pointPrev[1];
                        }
                    }
                } else if(datasetValuePrev >= datasetValue && datasetValueNext <= datasetValue) {
                    let diffY = Math.abs(100 / ((this.height - this.heightMargins) / (point[1] - pointNext[1])));

                    if(diffY >= threshold || datasetValueNext == datasetValue) {
                        position = 'bottom';
                        if(Math.abs(point[1] - pointPrev[1]) > yOffset + textHeight) {
                            y = pointPrev[1];
                        }
                    } else {
                        position = 'top';
                        if (Math.abs(point[1] - pointNext[1]) > yOffset + textHeight) {
                            y = pointNext[1];
                        }
                    }
                }
            }


        } else {
            if(!pointNext && datasetValuePrev < datasetValue) {
                position = 'bottom';
            }

            if(!pointPrev && datasetValueNext < datasetValue) {
                position = 'bottom';
            }
        }

        if(position === 'top') {
            y -= yOffset;
        } else {
            y += yOffset;
        }

        this.ctx.font = '15px Lato';
        let leftTextWidth = this.ctx.measureText(datasetValue).width - textSeparatorWidth;

        this.ctx.font = '10px Lato';
        let rightTextWidth = this.ctx.measureText(diff).width + textSeparatorWidth * 3;

        if(diff !== 0) {
            if(x - leftTextWidth < xMargin) {
                x = xMargin + leftTextWidth;
            }

            if(x + rightTextWidth > this.width - xMargin) {
                x = this.width - rightTextWidth;
            }
        } else {
            textSeparatorWidth = 0;
        }

        this.ctx.fillStyle = Chart.hexRgb('#ffffff');
        this.ctx.font = '15px Lato';
        this.ctx.textAlign = diff === 0 ? 'center' : 'right';
        this.ctx.fillText(datasetValue, x - textSeparatorWidth, y);

        if(diff !== 0) {
            this.ctx.fillStyle = Chart.hexRgb(diff > 0 ? '#e04965' : '#39cfb6');
            this.ctx.strokeStyle = Chart.hexRgb(diff > 0 ? '#e04965' : '#39cfb6');
            this.ctx.font = '10px Lato';
            this.ctx.textAlign = 'left';
            this.ctx.fillText(Math.abs(diff), x + textSeparatorWidth * 2, y - 4);

            this.drawTriangle(x + textSeparatorWidth, y - 11, diff < 0 ? 'top' : 'bottom');
        }
    }

    drawTriangle(x, y, dir = 'bottom') {
        let height = 7,
            width = 3;

        if(dir == 'top') {
            height = -height;
            width = -width;
            y -= height;
        }

        this.ctx.beginPath();
        this.ctx.moveTo(x, y);
        this.ctx.lineTo(x, y + height);
        this.ctx.lineTo(x + width, y + (height - width));
        this.ctx.moveTo(x, y + height);
        this.ctx.lineTo(x - width, y + (height - width));
        this.ctx.stroke();
        this.ctx.closePath();
    }

    getLinePoints(skipFakes = false) {
        let columnWidth = this.columnWidth,
            lineAreaHeight = this.height - this.heightMargins,
            values = this.values.slice(0),
            firstValue = values.slice(0, 1),
            lastValue = values.slice(-1);

        if(!skipFakes) {
            values.splice(0, 0, firstValue - firstValue * 0.2);
            values.push(lastValue - lastValue * 0.2);
        }

        return values.map((value, index) => {
            let offset = index - (skipFakes ? 0 : 1);
            return [columnWidth * offset + columnWidth / 2 , (lineAreaHeight / 100 * value) + this.options.marginTop];
        });
    }

    get columnWidth() {
        return this.width / this.values.length;
    }
}